<?php

class PeopleTaxonomy extends WebTaxonomy {

  /**
   * Implements WebTaxonomy::autocomplete().
   */
  public function autocomplete($string = '') {
    $matches = array();
    $web_tids = array();

    $url = "http://viaf.org/viaf/AutoSuggest";
    $params = array(
      'query' => $string,
    );

    return $this->fetchTermInfo($url, $params);
  }

  /**
   * TODO Implements WebTaxonomy::fetchTerm().
   *
   */
  public function fetchTerm($term) {

    $term_info = array();
    //drupal_set_message(t("term: %term",  array('%term' => $term)));
    $web_tid = $term->web_tid[LANGUAGE_NONE][0]['value'];
    $term_name = $term->name;
    //drupal_set_message(t("term_name: %term_name",  array('%term_name' => $term_name)));
    if (!empty($web_tid)) {
      $params = array();
      $term_info = $this->fetchTermInfoXML($web_tid, $term_name, $params);
    }
    else {
      // @todo If there is no Web Tid, check by name.
      drupal_set_message(t("No web_tid found."));
    }

    return array_shift($term_info);
  }

  protected function fetchTermInfo($url, $params) {
    $term_info = array();

    $http = http_client();
    $data = $http->get($url, $params);

    $json = json_decode($data);
    $results = $json->result;

    foreach ($results as $result) {

      $term = $result->term;
      $viafid = $result->viafid;
      $viafurl = "http://viaf.org/viaf/". $viafid ."/rdf.xml";

      $term_info[$term] = array(
        'name' => $term,
        'web_tid' => $viafurl,
      );
    }

    return $term_info;

  }

  /*
   * Get linked data from viaf using the web_tid
   */
  protected function fetchTermInfoXML($web_tid, $term, $params) {

    $term_info = array();
    $client = http_client();

    $data = $client->get($web_tid, $params);

    $xml = simplexml_load_string($data);
    if (version_compare(PHP_VERSION, '5.2.0', '>=')) {
      //Fetch all namespaces
      $namespaces = $xml->getNamespaces(true);
      //Register them with their prefixes
      foreach ($namespaces as $prefix => $ns) {
        $xml->registerXPathNamespace($prefix, $ns);
      }
    }

    $term_info[$term] = array(
      'name' => $term,
      'web_tid' => $web_tid,
    );

    return $term_info;
  }
}

